﻿namespace BrokereeSolutions.FileRestServer
{
    class ParseResult
    {
        public string FileName { get; set; }
        public Commands Action { get; set; }
        public FileType ConvertationTargetType { get; set; }
        public string Id { get; set; }
        public bool IsValid { get; set; }

    }
}
