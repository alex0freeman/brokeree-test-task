﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BrokereeSolutions.FileRestServer
{
    class CsvWriter
    {
        string ConvertRecordToString(byte[] rawData)
        {
            var record = new StringBuilder();
            //if (rawData.Length == 20)
            //{
            //    var ver = BitConverter.ToInt32(rawData, 0);
            //    var type = Encoding.UTF8.GetString(rawData, 4, 16);
            //    var headerString = $"{ver},{type}";
            //    record.AppendLine(headerString);
            //}
            if (rawData.Length == 80)
            {
                var id = BitConverter.ToInt32(rawData, 0);
                var account = BitConverter.ToInt32(rawData, 4);
                var volume = BitConverter.ToDouble(rawData, 8);
                var comment = Encoding.UTF8.GetString(rawData, 16, 64);
                var recordString = $"{id},{account},{volume},{comment}";
                record.AppendLine(recordString);
            }
            return record.ToString();
        }

        public void WriteToCsv(List<byte[]> records, string fileName)
        {
            using (var sw = File.AppendText(fileName))
            {
                foreach (var record in records)
                {
                    sw.Write(ConvertRecordToString(record));
                }
            }
        }

    }
}
