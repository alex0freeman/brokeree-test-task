﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BrokereeSolutions.FileRestServer
{
    class BinFileReader
    {
        private const int HeaderSize = 20;
        private const int RecordSize = 80;

        public List<byte[]> ReadBinaryFileToRecords(string fileName)
        {
            var records = new List<byte[]>();
            try
            {
                using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var br = new BinaryReader(fs, new ASCIIEncoding()))
                    {
                        var chunk = br.ReadBytes(HeaderSize);
                        while (chunk.Length > 0)
                        {
                            if (chunk.Length == RecordSize)
                            {
                                records.Add(chunk);
                            }
                            chunk = br.ReadBytes(RecordSize);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Binary file read error: " + ex);
            }
            return records;
        }
    }
}
