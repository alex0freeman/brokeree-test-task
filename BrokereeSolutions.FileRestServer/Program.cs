﻿using System;

namespace BrokereeSolutions.FileRestServer
{
    class Program
    {
        static string _address;
        static int _port;
        private const int MinPortRange = 1000;
        private const int MaxPortRange = 65535;

        static void Main()
        {
            var userDefinedAddress = GetServerAdressFromCommandLine();
            var srv = userDefinedAddress != null ? new RestServer(userDefinedAddress) : new RestServer();
            srv.Start();
        }

        static string GetServerAdressFromCommandLine()
        {
            try
            {
                var commandLineParapms = Environment.GetCommandLineArgs();
                if (commandLineParapms.Length == 3)
                {
                    _address = commandLineParapms[1];
                    _port = int.Parse(commandLineParapms[2]);
                }
                else
                {
                    return null;
                }

                if (_port > MinPortRange && _port < MaxPortRange)
                {
                    return $"{_address}:{_port}/";
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Commandline parameter error: " + ex);
            }

            return null;
        }
    }
}