﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using BrokereeSolutions.FileRestServer;

namespace BrokereeSolutions.FileRestServer
{
    internal class RestServer
    {
        private const string RootPath = "f:\\www\\";
        string _sqlbases = RootPath + "base\\";
        string _csv = RootPath + "csv\\";
        readonly string _serverAdress;
        private SqlDataProvider _sqlDataProvider;
        private CsvWriter _converter;
        private BinFileReader _binReader;
        private Logger _logger;

        private HttpListener _listener;

        public RestServer()
        {
            _binReader = new BinFileReader();
            _converter = new CsvWriter();
            _sqlDataProvider = new SqlDataProvider();
            _logger = new Logger();

            _serverAdress = "http://*:8080/";
        }

        public RestServer(string address) : this()
        {
            _serverAdress = address;
        }

        public void Start()
        {

            try
            {
                _listener = new HttpListener();
                var urlParser = new RestUrlParser();

                _listener.Prefixes.Add(_serverAdress);
                _listener.Start();
                Console.WriteLine("Listening on {0}...", _serverAdress);

                while (true)
                {
                    var context = _listener.GetContext();
                    var parseResult = urlParser.Parse(context.Request.Url);
                    if (parseResult.IsValid)
                    {
                        PerformAction(context, parseResult);
                        _logger.Log(context.Request.RemoteEndPoint.ToString(), parseResult);
                    }
                    else
                    {
                        ReturnStatus(context, (int)HttpStatusCode.BadRequest);
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("error: " + ex);
            }
            finally
            {
                _listener.Close();
            }
        }

        private void PerformAction(HttpListenerContext context, ParseResult parseResult)
        {
            switch (parseResult.Action)
            {
                case Commands.Convert:
                    Convert(context, parseResult);
                    break;
                case Commands.Download:
                    Download(context, parseResult);
                    break;
                case Commands.Get:
                    GetEntry(context, parseResult);
                    break;
                case Commands.Delete:
                    Delete(context, parseResult);
                    break;
                default:
                    ReturnStatus(context, (int)HttpStatusCode.NotFound);
                    break;
            }
        }

        #region REST Actions

        private void Convert(HttpListenerContext context, ParseResult pResult)
        {
            var fileName = pResult.FileName;
            var fullPath = RootPath + fileName;
            var csvfile = _csv + fileName;
            var sqlfile = _sqlbases + fileName;


            if (File.Exists(fullPath))
            {
                if (pResult.ConvertationTargetType == FileType.Csv)
                {
                    ConvertToCsv(fullPath, csvfile);
                }
                if (pResult.ConvertationTargetType == FileType.Sql)
                {
                    ConvertToSql(fullPath, sqlfile);
                }
            }
            else
            {
                ReturnStatus(context, (int)HttpStatusCode.NotFound);
            }
        }
        private void Download(HttpListenerContext context, ParseResult pResult)
        {
            var fileName = pResult.FileName;
            var csvfile = _csv + fileName;
            var sqlfile = _sqlbases + fileName;

            if (pResult.ConvertationTargetType == FileType.Csv && File.Exists(csvfile))
            {
                ReturnFile(context, csvfile);
            }
            else if (pResult.ConvertationTargetType == FileType.Sql && File.Exists(sqlfile))
            {
                ReturnFile(context, sqlfile);
            }
            else
            {
                ReturnStatus(context, (int)HttpStatusCode.NotFound);
            }
        }
        private void GetEntry(HttpListenerContext context, ParseResult pResult)
        {
            var fileName = pResult.FileName;
            var sqlfile = _sqlbases + fileName;

            if (File.Exists(sqlfile))
            {
                var entry = GetEntryFromSql(sqlfile, pResult.Id);
                if (entry != null)
                    RequestEntry(context, entry);
                else
                {
                    ReturnStatus(context, (int)HttpStatusCode.NotFound);
                }
            }
            else
            {
                ReturnStatus(context, (int)HttpStatusCode.BadRequest);
            }
        }
        private void Delete(HttpListenerContext context, ParseResult pResult)
        {
            var fileName = pResult.FileName;
            var csvfile = _csv + fileName;
            var sqlfile = _sqlbases + fileName;


            if (pResult.ConvertationTargetType == FileType.Csv && File.Exists(csvfile))
            {
                DeleteFile(csvfile);
                ReturnStatus(context, (int)HttpStatusCode.OK);
            }
            else if (pResult.ConvertationTargetType == FileType.Sql && File.Exists(sqlfile))
            {
                DeleteFile(sqlfile);
            }
            else
            {
                ReturnStatus(context, (int)HttpStatusCode.NotFound);
            }
        }
        #endregion

        private void ReturnFile(HttpListenerContext context, string filePath)
        {
            const int bufferSize = 1024 * 512;
            var response = context.Response;
            response.ContentType = "application/octet-stream";
            var buffer = new byte[bufferSize];

            using (var fs = File.OpenRead(filePath))
            {
                response.ContentLength64 = fs.Length;
                int read;
                while ((read = fs.Read(buffer, 0, buffer.Length)) > 0)
                    response.OutputStream.Write(buffer, 0, read);
            }

            response.OutputStream.Close();
        }

        private string GetEntryFromSql(string sqlfile, string request)
        {
            int id;
            if (int.TryParse(request, out id))
            {
                return _sqlDataProvider.FindRecord(id, sqlfile);
            }
            return null;
        }

        private void RequestEntry(HttpListenerContext context, string entry)
        {
            var response = context.Response;
            var entryBuffer = Encoding.ASCII.GetBytes(entry);
            response.ContentType = "text/plain";
            response.ContentLength64 = entry.Length;
            response.OutputStream.Write(entryBuffer, 0, entryBuffer.Length);

            response.OutputStream.Close();
        }

        private void ReturnStatus(HttpListenerContext context, int statusCode)
        {

            var response = context.Response;
            response.ContentType = "text/plain";

            response.StatusCode = statusCode;

        }

        private void DeleteFile(string file)
        {
            try
            {
                File.Delete(file);
            }
            catch (Exception ex)
            {
                Console.WriteLine("File deleting error: " + ex);
            }
        }

        private void ConvertToSql(string inputFile, string outputFile)
        {
            var records = _binReader.ReadBinaryFileToRecords(inputFile);
            _sqlDataProvider.CreateDbCompact(outputFile);
            _sqlDataProvider.WriteRecords(records, outputFile);
        }

        private void ConvertToCsv(string inputFile, string outputFile)
        {
            var records = _binReader.ReadBinaryFileToRecords(inputFile);
            _converter.WriteToCsv(records, outputFile);
        }
    }
}
