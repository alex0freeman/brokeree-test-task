﻿using System;
using System.IO;

namespace BrokereeSolutions.FileRestServer
{
    class Logger
    {
        string programPath = Directory.GetCurrentDirectory() + @"\log\";

        public Logger()
        {
            if (!Directory.Exists(programPath))
            {
                Directory.CreateDirectory(programPath);
            }
        }
        public void Log(string address, ParseResult parseResult)
        {
            var eventFile = programPath + "RestServerEventLog.txt";
            var log = $"Client address: {address}; request: {parseResult.Action}; requested file: {parseResult.FileName}; {DateTime.Now}\r\n";

            try
            {
                File.AppendAllText(eventFile, log);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Log write error: " + ex);
            }
        }

    }
}
