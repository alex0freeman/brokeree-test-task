﻿using System;
using System.Linq;

namespace BrokereeSolutions.FileRestServer
{
    enum Commands
    {
        Unspecified, Convert, Download, Get, Delete
    }

    enum FileType
    {
        Unspecified, Csv, Sql
    }

    class RestUrlParser
    {
        private const byte MinAddresParams = 3;
        private const byte MaxAddresParams = 8;
        private const byte Action = 4;
        private const byte File = 5;
        private const byte Format = 6;
        private const byte Id = 7;
        public ParseResult Parse(Uri url)
        {
            var parseResult = new ParseResult();

            var strParams = url.ToString().Split('/');
            if (Check(strParams))
            {
                var action = strParams[Action].ToUpper();
                switch (action)
                {
                    case "CONVERT":
                        parseResult.Action = Commands.Convert;
                        break;
                    case "DOWNLOAD":
                        parseResult.Action = Commands.Download;
                        break;
                    case "GET":
                        parseResult.Action = Commands.Get;
                        break;
                    case "DELETE":
                        parseResult.Action = Commands.Delete;
                        break;
                }

                parseResult.FileName = strParams[File];

                var fileFormat = strParams[Format].ToUpper();
                switch (fileFormat)
                {
                    case "CSV":
                        parseResult.ConvertationTargetType = FileType.Csv;
                        break;
                    case "SQL":
                        parseResult.ConvertationTargetType = FileType.Sql;
                        break;
                }

                parseResult.Id = strParams[Id];

                if (parseResult.Action != Commands.Unspecified)
                    parseResult.IsValid = true;
            }
            else
            {
                parseResult.IsValid = false;
            }
            return parseResult;
        }

        public bool Check(string[] str)
        {
            if (str.Length < MinAddresParams && str.Length > MaxAddresParams)
                return false;
            if (str[Action] == null && str[File] == null && str[Format] == null)
                return false;
            if (str[File].Any(new[] { '*', '<', '>', '|' }.Contains))
                return false;


            return true;
        }

    }
}
