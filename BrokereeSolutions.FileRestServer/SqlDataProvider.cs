﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.IO;
using System.Text;

namespace BrokereeSolutions.FileRestServer
{
    class SqlDataProvider
    {
        private SqlCeConnection _conn;

        public void CreateDbCompact(string baseName)
        {
            var connString = $"Data Source='{baseName}';";
            var engine = new SqlCeEngine(connString);

            if (!(File.Exists(baseName)))
            {
                try
                {
                    engine.CreateDatabase();
                    var connection = new SqlCeConnection(engine.LocalConnectionString);
                    connection.Open();
                    var command = connection.CreateCommand();
                    command.CommandText =
                        "CREATE TABLE Records (Id int NOT NULL, account int NOT NULL, volume float NOT NULL, comment nvarchar(64))";
                    command.ExecuteScalar();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error create database: " + ex);
                }
            }
        }

        public string FindRecord(int id, string baseName)
        {
            var connString = $"Data Source='{baseName}';";
            var entry = new StringBuilder();
            string dbid = null;
            string dbaccount = null;
            string dbvolume = null;
            string dbcomment = null;

            try
            {
                _conn = new SqlCeConnection(connString);
                _conn.Open();

                var command = _conn.CreateCommand();
                command.CommandText = "SELECT * FROM Records WHERE (id LIKE ?)";
                command.Parameters.AddWithValue("id", "%" + id + "%");
                var dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        var st = dataReader.GetValue(i).ToString();
                        switch (i)
                        {
                            case 0:
                                dbid = st;
                                break;
                            case 1:
                                dbaccount = st;
                                break;
                            case 2:
                                dbvolume = st;
                                break;
                            case 3:
                                dbcomment = st;
                                break;
                        }
                    }
                }
                dataReader.Close();
                command.Dispose();

                var newLine = $"{dbid},{dbaccount},{dbvolume},{dbcomment}";
                entry.AppendLine(newLine);


                return entry.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error read from Db: " + ex);
            }
            finally
            {
                _conn.Close();
            }

            return null;
        }

        public void WriteRecords(List<byte[]> recordsBytes, string baseName)
        {
            var connString = $"Data Source='{baseName}';";
            try
            {
                _conn = new SqlCeConnection(connString);
                _conn.Open();

                foreach (var recordByte in recordsBytes)
                {
                    if (recordByte.Length != 0)
                    {
                        var recordTuple = ConvertRecordToTuple(recordByte);
                        var command = _conn.CreateCommand();
                        //command.CommandText = $"INSERT INTO Records(id, account, volume, comment) " +
                        //                      $"VALUES({recordTuple.Item1},{recordTuple.Item2},{recordTuple.Item3},{recordTuple.Item4})";

                        command.CommandText = "INSERT INTO Records(id, account, volume, comment) VALUES(?,?,?,?)";
                        command.Parameters.AddWithValue("id", recordTuple.Item1);
                        command.Parameters.AddWithValue("account", recordTuple.Item2);
                        command.Parameters.AddWithValue("volume", recordTuple.Item3);
                        command.Parameters.AddWithValue("comment", recordTuple.Item4);
                        command.ExecuteScalar();
                        command.Dispose();
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error write to Db: " + ex);
            }
            finally
            {
                _conn.Close();
            }
        }

        private Tuple<int, int, double, string> ConvertRecordToTuple(byte[] rawData)
        {
            Tuple<int, int, double, string> tmp = null;

            if (rawData.Length == 80)
            {
                var id = BitConverter.ToInt32(rawData, 0);
                var account = BitConverter.ToInt32(rawData, 4);
                var volume = BitConverter.ToDouble(rawData, 8);
                var comment = Encoding.UTF8.GetString(rawData, 16, 64);

                tmp = new Tuple<int, int, double, string>(id, account, volume, comment);
            }

            return tmp;
        }

    }
}
